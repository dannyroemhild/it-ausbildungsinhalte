# Allgemein

Vor der Durchf�hrung des Abschlussprojekts und dessen anschlie�ende Pr�sentation steht erst einmal die H�rde des Antrags.

Hiervor muss man keine Angst haben, wenn man sich vorallem formal ein paar Regeln vor Augen f�hrt, die allzu oft nicht eingehalten werden und deshalb so mancher Projektantrag aufgrund von Formm�ngeln abgelehnt wird.

## Gesamtdauer: 40 Stunden
Dies ist in der "Verordnung �ber die Berufsausbildung zum Fachinformatiker und zur Fachinformatikerin (Fachinformatikerausbildungsverordnung - FIAusbV)" vom 28.02.2020 �20 "Pr�fungsbereich Planen und Umsetzen eines Projektes der Systemintegration" f�r Fachinformatiker Fachrichtung Systemintegration festgelegt und muss zwingend eingehalten werden.

* https://www.gesetze-im-internet.de/fiausbv/BJNR025000020.html

## Zeitplanung: >=50% der Gesamtzeit Projektdurchf�hrung

Inhatlich werden bereits die groben "Teilabschnitte" des Abschlussprojekts vorgegeben / benannt:

1. auftragsbezogene Anforderungen zu analysieren,
2. L�sungsalternativen unter Ber�cksichtigung technischer, wirtschaftlicher und qualitativer Aspekte vorzuschlagen,
3. System�nderungen und -erweiterungen durchzuf�hren und zu �bergeben,
4. IT-Systeme einzuf�hren und zu pflegen,
5. Schwachstellen von IT-Systemen zu analysieren und Schutzma�nahmen vorzuschlagen und umzusetzen sowie
6. Projekte der Systemintegration anforderungsgerecht zu dokumentieren.

Die Durchf�hrung sollte zeitlich den Gro�teil ( ca 50%) des Projekts einnehmen.

## Zeitplanung: max. 25% der Gesamtzeit Planungsphase
Die Planungsphase (IST-Aufnahme, SOLL-Konzept, Abstimmungstermine, Informationssammlung, Angebotserstellung, etc) sollte nicht mehr als 1/4 des Projekts einnehmen.

## Zeitplanung: max. 25% der Gesamtzeit Projektabschluss
Die Dokumentation des Projekts sollte maximal mit 8 Stunden in der Zeitplanung veranschlagt werden. Andere Phasen wie "�bergabe an Kunden" oder "Einweisung der Mitarbeiter" sollten die Gesamtzeit des Projektabschlusses auf maximal 10 Zeitstunden erg�nzen.

## Angemessene fachliche Tiefe vs. Zeitrahmen
Man sollte unbedingt darauf achten, dass das Thema eine angemessene fachliche Tiefe hat. Falls man sich hierbei nicht sicher ist, sollte man dringend mit dem eigenen Ausbilder oder Kollegen (welche bereits die Abschl�sspr�fung erfolgreich abgeschlossen haben) dar�ber sprechen. Da die Pr�fer ausschlie�lich anhand des Inhaltes des Projektantrags hier�ber entscheiden m�ssen, sollte in diesem entsprechend ausf�hrlich auf die geplanten einzelnen Schritte des Projektes eingegangen werden. Sollten im Vorfeld Faktoren erkennbar sein, die ein eigentlich einfaches Projekt zus�tzlich erschweren (aufgrund heterogener Infrastruktur oder �hnlichem) bitte unbedingt darlegen.

## Selbst�ndigkeit
Bei dem Abschlussprojekt soll festgestellt werden, dass der Pr�fling mit Projektsituationen umgehen und eine fachlich qualifizierte L�sung finden und umsetzen kann. Projektantr�ge aus denen hervorgeht, dass der Gro�teil der T�tigkeiten durch Kollegen oder gar andere Abteilungen durchgef�hrt wird, hat wohl kaum eine Chance zugelassen zu werden.